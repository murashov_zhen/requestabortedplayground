﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace RequestAbortedNetCore1_1.Controllers
{
	[Route("[controller]")]
	public class DemoController : Controller
	{
		private readonly ILogger<DemoController> _logger;

		public DemoController(ILogger<DemoController> logger)
		{
			_logger = logger;
		}


		[HttpGet]
		public IActionResult Get(CancellationToken cancellationToken, int delay = 30)
		{
			var str = string.Join("", Enumerable.Repeat("*", 20));

			_logger.LogInformation($"{str}Starting to do slow work. With delay - {delay} seconds");

			try
			{
				var n = 10;
				for (var i = 0; i < n; i++)
				{
					_logger.LogInformation($"HttpContext.RequestAborted.IsCancellationRequested - {HttpContext.RequestAborted.IsCancellationRequested}");
					cancellationToken.ThrowIfCancellationRequested();
					
					Thread.Sleep(TimeSpan.FromSeconds(delay / n));
				}
			}
			catch (OperationCanceledException e)
			{
				_logger.LogError($"{str}Request was aborted");
				return StatusCode(499);
			}


			_logger.LogInformation($"{str}Finished slow delay of {delay} seconds.");
			return Ok();
		}
	}
}
# Запуск 
```
cd repository_path
docker-compose up --build
```
# Проверка Net core 1.1  
#### Успешное выполнение: Get запрос http://net_core_1_1.localtest.me/demo?delay=5
* В логах должны увидеть сообщение `********************Starting to do slow work. With delay - 5 seconds`
* В логах должны увидеть сообщение `********************Finished slow delay of 5 seconds.`
* Должны получить 200 статус

#### Проверка разрыва: Get запрос http://net_core_1_1.localtest.me/demo?delay=30. До истечения 30 секунд прервать запрос
* В логах должны увидеть сообщение `********************Starting to do slow work. With delay - 30 seconds`
* В логах должны увидеть сообщение `********************Request was aborted`
* В логах не должно быть сообщения `********************Finished slow delay of 30 seconds.`

_Получаем что nginx возвращает 499 статус. И процесс продолжается в логах сообщение_ `********************Finished slow delay of 30 seconds.`

# Проверка Net core 2.1  
#### Успешное выполнение: Get запрос http://net_core_2_1.localtest.me/demo?delay=5
* В логах должны увидеть сообщение `********************Starting to do slow work. With delay - 5 seconds`
* В логах должны увидеть сообщение `********************Finished slow delay of 5 seconds.`
* Должны получить 200 статус

#### Проверка разрыва: Get запрос http://net_core_2_1.localtest.me/demo?delay=30. До истечения 30 секунд прервать запрос
* В логах должны увидеть сообщение `********************Starting to do slow work. With delay - 30 seconds`
* В логах должны увидеть сообщение `********************Request was aborted`
* В логах не должно быть сообщения `********************Finished slow delay of 30 seconds.`

_Все ок запрос прерван_ `********************Request was aborted`
